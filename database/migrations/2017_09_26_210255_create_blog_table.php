<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title')->nullable();
            $table->integer('featured')->nullable()->default("0");

            $table->string('category', 50)->nullable()->default("Uncategorized");  
            $table->enum('status',["draft","published"])->default("draft");
            $table->longText('content')->nullable();

            $table->text('path')->nullable();
            $table->text('directory')->nullable();
            $table->text('filename')->nullable();

            $table->dateTime('posted_at');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blog');
    }
}
