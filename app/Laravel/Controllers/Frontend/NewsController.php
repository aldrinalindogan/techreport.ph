<?php 

namespace App\Laravel\Controllers\Frontend;

/*
*
* Models used for this controller
*/
use App\User;
use App\Laravel\Models\News;

/*
*
* Requests used for validating inputs
*/


/*
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, DB;

class NewsController extends Controller{

	/*
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	}

	public function index () {
		$this->data['news'] = News::orderBy('posted_at','DESC')->paginate(9);
		return view('frontend.news.index',$this->data);
	}

	public function show ($id = NULL) {
		$this->data['current_news'] = News::find($id);
		return view('frontend.news.show',$this->data);
	}
}