<?php 

namespace App\Laravel\Controllers\Frontend;

/*
*
* Models used for this controller
*/
use App\User;
use App\Laravel\Models\Partner;

/*
*
* Requests used for validating inputs
*/


/*
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, DB;

class PartnerController extends Controller{

	/*
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	}

	public function index ($type = NULL) {
		$this->data['partners'] = Partner::where('type',$type)->get();
		return view('frontend._pages.partners',$this->data);
	}
}