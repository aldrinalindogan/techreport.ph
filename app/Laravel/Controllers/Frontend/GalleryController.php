<?php 

namespace App\Laravel\Controllers\Frontend;

/*
*
* Models used for this controller
*/
use App\User;
use App\Laravel\Models\Album;
use App\Laravel\Models\Gallery;

/*
*
* Requests used for validating inputs
*/


/*
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, DB;

class GalleryController extends Controller{

	/*
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	}

	public function index () {
		$this->data['albums'] = Album::orderBy('posted_at','DESC')->get();
		return view('frontend.gallery.index',$this->data);
	}

	public function show ($id = NULL) {
		$this->data['album'] = Album::find($id);
		$this->data['images'] = Gallery::where('album_id',$id)->get();
		
		return view('frontend.gallery.show',$this->data);
	}
}