<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\PageContent;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\PageContentRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class PageContentController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Page Content";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "page_content";

		$this->data['page_locations'] = ['' => "Choose page location", 'asean_bac' => "ASEAN BAC", 'aba' => "ABA", 'about_abac_footer' => "ABOUT ABAC FOOTER", 'members' => "MEMBERS", 'abis' => "ABIS"];
	}

	public function index () {
		$this->data['page_content'] = PageContent::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (PageContentRequest $request) {
		try {
			$new_page_content = new PageContent;
			$new_page_content->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/page_content');
				$new_page_content->path = $upload["path"];
				$new_page_content->directory = $upload["directory"];
				$new_page_content->filename = $upload["filename"];
			}

			if($new_page_content->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New page content has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$page_content = PageContent::find($id);

		if (!$page_content) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['page_content'] = $page_content;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (PageContentRequest $request, $id = NULL) {
		try {
			$page_content = PageContent::find($id);

			if (!$page_content) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$page_content->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/page_content');
				if($upload){	
					if (File::exists("{$page_content->directory}/{$page_content->filename}")){
						File::delete("{$page_content->directory}/{$page_content->filename}");
					}
					if (File::exists("{$page_content->directory}/resized/{$page_content->filename}")){
						File::delete("{$page_content->directory}/resized/{$page_content->filename}");
					}
					if (File::exists("{$page_content->directory}/thumbnails/{$page_content->filename}")){
						File::delete("{$page_content->directory}/thumbnails/{$page_content->filename}");
					}
				}
				
				$page_content->path = $upload["path"];
				$page_content->directory = $upload["directory"];
				$page_content->filename = $upload["filename"];
			}

			if($page_content->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A page content has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$page_content = PageContent::find($id);

			if (!$page_content) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$page_content->directory}/{$page_content->filename}")){
				File::delete("{$page_content->directory}/{$page_content->filename}");
			}
			if (File::exists("{$page_content->directory}/resized/{$page_content->filename}")){
				File::delete("{$page_content->directory}/resized/{$page_content->filename}");
			}
			if (File::exists("{$page_content->directory}/thumbnails/{$page_content->filename}")){
				File::delete("{$page_content->directory}/thumbnails/{$page_content->filename}");
			}

			if($page_content->save() AND $page_content->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A page content has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}