<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Testimonial;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\TestimonialRequest;
use App\Laravel\Requests\Backoffice\EditTestimonialRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class TestimonialController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Testimonial";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "testimonials";
		$this->data['types'] = [''=>'Choose Testimonial type','bronze'=>"Bronze",'platinum'=>"Platinum"];
	}

	public function index () {
		$this->data['testimonial'] = Testimonial::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (TestimonialRequest $request) {
		try {
			$new_testimonial = new Testimonial;
			$new_testimonial->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/testimonial');
				$new_testimonial->path = $upload["path"];
				$new_testimonial->directory = $upload["directory"];
				$new_testimonial->filename = $upload["filename"];
			}

			if($new_testimonial->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New Testimonial has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$testimonial = Testimonial::find($id);

		if (!$testimonial) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['testimonial'] = $testimonial;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (EditTestimonialRequest $request, $id = NULL) {
		try {
			$testimonial = Testimonial::find($id);

			if (!$testimonial) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$testimonial->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/testimonial');
				if($upload){	
					if (File::exists("{$testimonial->directory}/{$testimonial->filename}")){
						File::delete("{$testimonial->directory}/{$testimonial->filename}");
					}
					if (File::exists("{$testimonial->directory}/resized/{$testimonial->filename}")){
						File::delete("{$testimonial->directory}/resized/{$testimonial->filename}");
					}
					if (File::exists("{$testimonial->directory}/thumbnails/{$testimonial->filename}")){
						File::delete("{$testimonial->directory}/thumbnails/{$testimonial->filename}");
					}
				}
				
				$testimonial->path = $upload["path"];
				$testimonial->directory = $upload["directory"];
				$testimonial->filename = $upload["filename"];
			}

			if($testimonial->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A Testimonial has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$testimonial = Testimonial::find($id);

			if (!$testimonial) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$Testimonials->directory}/{$Testimonials->filename}")){
				File::delete("{$Testimonials->directory}/{$Testimonials->filename}");
			}
			if (File::exists("{$Testimonials->directory}/resized/{$Testimonials->filename}")){
				File::delete("{$Testimonials->directory}/resized/{$Testimonials->filename}");
			}
			if (File::exists("{$Testimonials->directory}/thumbnails/{$Testimonials->filename}")){
				File::delete("{$Testimonials->directory}/thumbnails/{$Testimonials->filename}");
			}

			if($testimonial->save() AND $testimonial->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A Testimonial has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}