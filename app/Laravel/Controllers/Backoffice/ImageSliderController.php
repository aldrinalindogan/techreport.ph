<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\ImageSlider;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\ImageSliderRequest;
use App\Laravel\Requests\Backoffice\EditImageSliderRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class ImageSliderController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "image_slider";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "image_slider";
	}

	public function index () {
		$this->data['imageslider'] = ImageSlider::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (ImageSliderRequest $request) {
		try {
			$new_imageslider = new ImageSlider;
			$new_imageslider->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/imageslider');
				$new_imageslider->path = $upload["path"];
				$new_imageslider->directory = $upload["directory"];
				$new_imageslider->filename = $upload["filename"];
			}

			if($new_imageslider->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New partner has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$imageslider = ImageSlider::find($id);

		if (!$imageslider) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['imageslider'] = $imageslider;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (EditImageSliderRequest $request, $id = NULL) {
		try {
			$imageslider = ImageSlider::find($id);

			if (!$imageslider) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$imageslider->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/imageslider');
				if($upload){	
					if (File::exists("{$imageslider->directory}/{$imageslider->filename}")){
						File::delete("{$imageslider->directory}/{$imageslider->filename}");
					}
					if (File::exists("{$imageslider->directory}/resized/{$imageslider->filename}")){
						File::delete("{$imageslider->directory}/resized/{$imageslider->filename}");
					}
					if (File::exists("{$imageslider->directory}/thumbnails/{$imageslider->filename}")){
						File::delete("{$imageslider->directory}/thumbnails/{$imageslider->filename}");
					}
				}
				
				$imageslider->path = $upload["path"];
				$imageslider->directory = $upload["directory"];
				$imageslider->filename = $upload["filename"];
			}

			if($imageslider->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A partner has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$imageslider = ImageSlider::find($id);

			if (!$imageslider) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$imageslider->directory}/{$imageslider->filename}")){
				File::delete("{$imageslider->directory}/{$imageslider->filename}");
			}
			if (File::exists("{$imageslider->directory}/resized/{$imageslider->filename}")){
				File::delete("{$imageslider->directory}/resized/{$imageslider->filename}");
			}
			if (File::exists("{$imageslider->directory}/thumbnails/{$imageslider->filename}")){
				File::delete("{$imageslider->directory}/thumbnails/{$imageslider->filename}");
			}

			if($imageslider->save() AND $imageslider->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A partner has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}