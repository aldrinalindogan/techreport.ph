@extends('backoffice._layouts.main')
@section('content')
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-calendar"></i> <span class="text-semibold">Events</span> - Create a new events.</h4>
        </div>
        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="{{route('backoffice.events.index')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack text-primary"></i><span>All Data</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href=""><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="">Events</a></li>
            <li class="active">Create</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">   
    <form id="target" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Event Details</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <!-- <li><a data-action="reload"></a></li> -->
                        <!-- <li><a data-action="close"></a></li> -->
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                
                <p class="content-group-lg">Below are the general information for this user.</p>
                
                <div class="form-group {{$errors->first('title') ? 'has-error' : NULL}}">
                    <label for="title" class="control-label col-lg-2 text-right">Title<span class="text-danger"> *</span></label>
                    <div class="col-lg-9">
                        <input class="form-control" type="text" name="title" id="title" placeholder="" maxlength="100" value="{{old('title')}}">
                        @if($errors->first('title'))
                        <span class="help-block">{{$errors->first('title')}}</span>
                        @endif
                    </div>
                </div>                
                
                <div class="form-group {{$errors->first('sub_title') ? 'has-error' : NULL}}">
                    <label for="sub_title" class="control-label col-lg-2 text-right">Sub Title<span class="text-danger"> *</span></label>
                    <div class="col-lg-9">
                        <input class="form-control" type="text" name="sub_title" id="sub_title" placeholder="" maxlength="100" value="{{old('sub_title')}}">
                        @if($errors->first('sub_title'))
                        <span class="help-block">{{$errors->first('sub_title')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group {{$errors->first('excerpt') ? 'has-error' : NULL}}">
                    <label for="excerpt" class="control-label col-lg-2 text-right">Excerpt <span class="text-danger"> *</span></label>
                    <div class="col-lg-9">
                        <input class="form-control" type="text" name="excerpt" id="excerpt" placeholder="" maxlength="100" value="{{old('excerpt')}}">
                        @if($errors->first('excerpt'))
                        <span class="help-block">{{$errors->first('excerpt')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group {{$errors->first('details') ? 'has-error' : NULL}}">
                    <label for="details" class="control-label col-lg-2 text-right">Details <span class="text-danger"> *</span></label>
                    <div class="col-lg-9">
                        <textarea class="form-control summernote" value="" placeholder="" type="text" name="details">{{old('details')}}</textarea>
                        @if($errors->first('details'))
                        <span class="help-block">{{$errors->first('details')}}</span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group {{$errors->first('daterange') ? 'has-error' : NULL}}">
                    <label for="content" class="control-label col-lg-2 text-right">Date Range</label>
                    <div class="col-lg-9">
                        <input class="fdropup form-control daterange-single" value="{{old('daterange')}}" placeholder="Enter Posted At" type="date" name="daterange">
                        <span class="help-block">This field is optional.</span>
                        @if($errors->first('daterange'))
                        <span class="help-block">{{$errors->first('daterange')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group {{$errors->first('address') ? 'has-error' : NULL}}">
                    <label for="address" class="control-label col-lg-2 text-right">Event Location<span class="text-danger"> *</span></label>
                    <div class="col-lg-9">
                        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCVjVd27vCiyVgWbRlAODLLRC8VpbN2-zc&libraries=places"></script>
                        <input id="pac-input" class="controls form-control mt-1 {{$errors->first('address') ? 'border-danger' : NULL}}" type="text" name="address" value="{{old('address')}}" placeholder="" style="width: 30%;" data-toggle="tooltip" title="{{$errors->first('address') ? $errors->first('address') : NULL}}">
                        @if($errors->first('address'))
                        <span class="help-block">{{$errors->first('address')}}</span>
                        @endif
                    </div>
                </div>



                <div class="form-group {{$errors->first('file') ? 'has-error' : NULL}}">
                    <label class="control-label col-lg-2 text-right">Upload avatar</label>
                    <div class="col-lg-9">
                        <input type="file" name="file" class="file-styled-primary" data-multiple-caption="{count} files selected" multiple="" class="inputfile">
                        @if($errors->first('file'))
                        <span class="help-block">{{$errors->first('file')}}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="content-group">
            <div class="text-left">
                <button id="save" type="submit" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Saving ..." class="btn btn-primary btn-raised btn-lg btn-loading">Save</button>
                &nbsp;
                <a type="button" class="btn btn-default btn-raised btn-lg" href="{{route('backoffice.users.index')}}">Cancel</a>
            </div>
        </div>
    </form>
    @include('backoffice._components.footer')
</div>
<!-- /content area -->
@stop
@section('modals')
@stop
@section('page-styles')
@stop
@section('page-scripts')
@include('backoffice._includes.page-jgrowl')
<script type="text/javascript" src="{{asset('backoffice/js/pages/components_popups.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/spin.min.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/editors/summernote/summernote.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/ladda.min.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/styling/uniform.min.js')}}"></script>

<!-- Select2 -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/selects/select2.min.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/pages/form_inputs.js')}}"></script>

<!-- Daterange Picker -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/ui/moment/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/pickers/daterangepicker.js')}}"></script>

<!-- CKEditor -->
 
<script type="text/javascript">
    $(function(){

        $(".styled, .multiselect-container input").uniform({
            radioClass: 'choice'
        });

        $('.btn-loading').click(function () {
            var btn = $(this);
            btn.button('loading');
        });

        $(".styled, .multiselect-container input").uniform({
            radioClass: 'choice'
        });

        $('.select').each(function(){
            $id = "#" + $(this).attr('id') + " option:first";
            $($id).prop('disabled',1);
        });

        $('.select-no-search').select2({
            minimumResultsForSearch: Infinity
        });

        $('.select-with-search').select2();

        $('#birthdate').daterangepicker({ 
            autoApply: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: false,
            showDropdowns: true,
            locale: {
                format: 'YYYY-MM-DD'
            }
        }).on('apply.daterangepicker', function (ev, picker){
            $(this).val(picker.startDate.format("YYYY-MM-DD"));
        });

    });
</script>
<script type="text/javascript">
    $(function(){
        $('#content').summernote({
            height: 400,
            onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
            }
        });

         function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);
            data.append("api_token","{{env('APP_TOKEN','base64:Lhp1BB3ms7WVgXBKbOkSmDQaIYlCQu/sXfV1Tp2woR0=')}}");
            $.ajax({
                data: data,
                type: "POST",
                url: "{{route('api.summernote',['json'])}}",
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                    if(data.status == true){
                        $('#content').summernote('insertImage', data.image);
                    }
                }
            });
        }

    });
</script>

<script type="text/javascript">
     function init() {
   var map = new google.maps.Map(document.getElementById('map-canvas'), {
     center: {
       lat: 14.599512,
       lng: 120.984222
     },
     zoom: 12
   });


   var searchBox = new google.maps.places.SearchBox(document.getElementById('pac-input'));
   map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.getElementById('pac-input'));
   google.maps.event.addListener(searchBox, 'places_changed', function() {
     searchBox.set('map', null);


     var places = searchBox.getPlaces();

     var bounds = new google.maps.LatLngBounds();
     var i, place;
     for (i = 0; place = places[i]; i++) {
       (function(place) {
         var marker = new google.maps.Marker({

           position: place.geometry.location
         });
         marker.bindTo('map', searchBox, 'map');
         google.maps.event.addListener(marker, 'map_changed', function() {
           if (!this.getMap()) {
             this.unbindAll();
           }
         });
         bounds.extend(place.geometry.location);


       }(place));

     }
     map.fitBounds(bounds);
     searchBox.set('map', map);
     map.setZoom(Math.min(map.getZoom(),12));

   });
 }
 google.maps.event.addDomListener(window, 'load', init);
</script>
@stop

 

    