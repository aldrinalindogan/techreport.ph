@extends('backoffice._layouts.main')
@section('content')
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="glyphicon glyphicon-picture"></i> <span class="text-semibold">Blog</span> - Create a new Blog.</h4>
        </div>
        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="{{route('backoffice.blog.index')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack text-primary"></i><span>All Data</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href=""><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="">Blog</a></li>
            <li class="active">Create</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <form id="target" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Blog Details</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <!-- <li><a data-action="reload"></a></li> -->
                        <!-- <li><a data-action="close"></a></li> -->
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                
                <p class="content-group-lg">Below are the general information for this user.</p>

                <div class="form-group {{$errors->first('title') ? 'has-error' : NULL}}">
                    <label for="type" class="control-label col-lg-2 text-right">Title<span class="text-danger"></span></label>
                    <div class="col-lg-9">
                        <input type="text" id="title" name="title" class="dropup form-control daterange-single" placeholder="" value="{{old('title', $blog->title)}}">
                        @if($errors->first('title'))
                        <span class="help-block">{{$errors->first('title')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group pl-10">
                    <div class="checkbox col-lg-offset-2 col-lg-5">
                        <label>
                            <input type="checkbox" name="featured" class="styled" value="1" {{old('featured') == "1" ? 'checked' : NULL}}>
                            Is featured?
                            <span class="help-block">A featured blog appears in Blog section.</span>
                        </label>
                    </div>
                </div>

                <div class="form-group {{$errors->first('status') ? 'has-error' : NULL}}">
                    <label for="status" class="control-label col-lg-2 text-right">Status <span class="text-danger"></span></label>
                    <div class="col-lg-9">
                        {!!Form::select("status", $status, old('status',$blog->status), ['id' => "status", 'class' => "select select-no-search col-xs-12 col-sm-12 col-md-12 col-lg-5"])!!}
                        @if($errors->first('status'))
                        <span class="help-block">{{$errors->first('status')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group {{$errors->first('posted_at') ? 'has-error' : NULL}}">
                    <label for="content" class="control-label col-lg-2 text-right">Posted At</label>
                    <div class="col-lg-9">
                        <input class="fdropup form-control daterange-single" value="{{old('posted_at', $blog->posted_at)}}" placeholder="" type="date" name="posted_at">
                        <span class="help-block">This field is optional.</span>
                        @if($errors->first('posted_at'))
                        <span class="help-block">{{$errors->first('posted_at')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group {{$errors->first('content') ? 'has-error' : NULL}}">
                    <label for="content" class="control-label col-lg-2 text-right">Content</label>
                    <div class="col-lg-9">
                        <textarea class="form-control summernote " value="" placeholder="" type="text" name="content">{{old('content', $blog->content)}}</textarea>
                        <span class="help-block">This field is optional.</span>
                        @if($errors->first('link'))
                        <span class="help-block">{{$errors->first('content')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2 text-right">Current Thumbnail</label>
                    <div class="col-lg-9">
                    <img src="{{asset($blog->directory.'/'.$blog->filename)}}" alt="" class="img-thumbnail" width="300"> </div>               
                </div>


                <div class="form-group {{$errors->first('file') ? 'has-error' : NULL}}">
                    <label class="control-label col-lg-2 text-right">Choose Thumbnail</label>
                    <div class="col-lg-9">
                        <input type="file" name="file" class="file-styled-primary" data-multiple-caption="{count} files selected" multiple="" class="inputfile">
                        @if($errors->first('file'))
                        <span class="help-block">{{$errors->first('file')}}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="content-group">
            <div class="text-left">
                <button id="save" type="submit" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Saving ..." class="btn btn-primary btn-raised btn-lg btn-loading">Save</button>
                &nbsp;
                <a type="button" class="btn btn-default btn-raised btn-lg" href="{{route('backoffice.users.index')}}">Cancel</a>
            </div>
        </div>
    </form>
    @include('backoffice._components.footer')
</div>
<!-- /content area -->
@stop
@section('modals')
@stop
@section('page-styles')
@stop
@section('page-scripts')
@include('backoffice._includes.page-jgrowl')
<script type="text/javascript" src="{{asset('backoffice/js/pages/components_popups.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/spin.min.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/editors/summernote/summernote.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/ladda.min.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/styling/uniform.min.js')}}"></script>

<!-- Select2 -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/selects/select2.min.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/pages/form_inputs.js')}}"></script>

<!-- Daterange Picker -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/ui/moment/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/pickers/daterangepicker.js')}}"></script>


<!-- CKEditor -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/editors/ckeditor/ckeditor.js')}}"></script>


<script type="text/javascript">
    $(function(){

        $(".styled, .multiselect-container input").uniform({
            radioClass: 'choice'
        });

        $('.btn-loading').click(function () {
            var btn = $(this);
            btn.button('loading');
        });

        $(".styled, .multiselect-container input").uniform({
            radioClass: 'choice'
        });

        $('.select').each(function(){
            $id = "#" + $(this).attr('id') + " option:first";
            $($id).prop('disabled',1);
        });

        $('.select-no-search').select2({
            minimumResultsForSearch: Infinity
        });

        $('.select-with-search').select2();

        $('#birthdate').daterangepicker({ 
            autoApply: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: false,
            showDropdowns: true,
            locale: {
                format: 'YYYY-MM-DD'
            }
        }).on('apply.daterangepicker', function (ev, picker){
            $(this).val(picker.startDate.format("YYYY-MM-DD"));
        });

    });
</script>
@stop

 