    <li class="">
        <a href=""><i class="icon-home4"></i> <span>Dashboard</span></a>
    </li>
    
    <!-- Menu -->
    <li class="navigation-header"><span>Menu</span> <i class="icon-menu" title="User Management"></i></li>

    <li class="">
        <a href="{{route('backoffice.contact_inquiry.index')}}"><i class="icon-phone"></i> <span>Contact Inquiries</span></a>
    </li>
    <li class="">
        <a href="{{route('backoffice.subscribers.index')}}"><i class="glyphicon glyphicon-thumbs-up"></i> <span>Subscribers</span></a>
    </li>
    <li class="">
        <a href="{{route('backoffice.question.index')}}"><i class="glyphicon glyphicon-question-sign"></i> <span>Questions</span></a>
    </li>
    <li class="">
        <a href="{{route('backoffice.blog.index')}}"><i class="glyphicon glyphicon-question-sign"></i> <span>Blogs</span></a>
    </li>
    <!-- /Menu -->

    <li class="navigation-header"><span>Web Content</span> <i class="icon-menu" title="User Management"></i></li>
    <li class = "">
        <a href="#"><i class="glyphicon glyphicon-globe"></i> <span>Web Content</span></a>
       
        <ul>
        <li class="">
            <a href="{{route('backoffice.social_links.index')}}"><i class="glyphicon glyphicon-link"></i><span>Social Links</span></a>
        </li>

        <li class="">
            <a href="{{route('backoffice.image_slider.index')}}"><i class="glyphicon glyphicon-picture"></i> <span>Image Slider</span></a>
        </li>

        <li class="">
            <a href="{{route('backoffice.faq.index')}}"><i class="glyphicon glyphicon-question-sign"></i> <span>FAQ's</span></a>
        </li>

        <li class="">
            <a href="{{route('backoffice.testimonials.index')}}"><i class="glyphicon glyphicon-user"></i> <span>Testimonials</span></a>
        </li>

        <li class="">
            <a href="{{route('backoffice.partners.index')}}"><i class="icon-users"></i> <span>Partners</span></a>
        </li>

        <li class="">
            <a href="{{route('backoffice.awards.index')}}"><i class="fa fa-trophy"></i> <span>Awards</span></a>
        </li>

        <li class="">
            <a href=""><i class="icon-wrench"></i> <span>Services</span></a>
        </li>    
        
        <li class="">
            <a href="{{route('backoffice.page_content.index')}}"><i class="icon-certificate"></i> <span>Page Content</span></a>
        </li>    
        
        <li class="">
            <a href="{{route('backoffice.news.index')}}"><i class="icon-newspaper"></i> <span>News</span></a>
        </li>
        
        <li class="">
            <a href="{{route('backoffice.events.index')}}"><i class="icon-calendar"></i> <span>Events</span></a>
        </li>
        
        </ul>
    </li>
    <li class="navigation-header"><span>User Management</span> <i class="icon-menu" title="User Management"></i></li>
    
    <li class="">
        <a href="#"><i class="icon-users"></i> <span>Administrator Accounts</span></a>
        <ul>
            <li class=""><a href="{{route('backoffice.users.index')}}">Record Data</a></li>
            <li class=""><a href="{{route('backoffice.users.create')}}">Create New</a></li>
        </ul>
    </li>


 