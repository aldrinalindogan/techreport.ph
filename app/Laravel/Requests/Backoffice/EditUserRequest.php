<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth;	
use App\Laravel\Requests\RequestManager;

class EditUserRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;

		$rules = [
			'type' => "required",
			'fname' => "required",
			'lname' => "required",
			'address' => "required",
			'gender' => "required",
			'email' => "required",
			'password' => "required|confirmed|between:6,25",
		];

		if($id){
			unset($rules['password']);
		}

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
			'password.between' => "Password must be min. of 6 and max. of 25 characters.",
			'password.confirmed' => "Password does not match.",
		];
	}
}