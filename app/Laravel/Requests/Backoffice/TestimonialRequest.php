<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth, Input;
use App\Laravel\Requests\RequestManager;

class TestimonialRequest extends RequestManager{

	public function rules(){

		$rules = [
			'author' => "required",
			'position' => "required",
			'message' => "required",
			'file' => "required|image",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This field is required.",
		];
	}
}