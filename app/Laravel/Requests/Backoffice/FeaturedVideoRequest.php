<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth, Input;
use App\Laravel\Requests\RequestManager;

class FeaturedVideoRequest extends RequestManager{

	public function rules(){

		$rules = [
			'title' => "required",
			'embedded_link' => "required",
			'file' => "image|required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This field is required.",
		];
	}
}