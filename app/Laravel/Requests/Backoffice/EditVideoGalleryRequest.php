<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth, Input;
use App\Laravel\Requests\RequestManager;

class EditVideoGalleryRequest extends RequestManager{

	public function rules(){

		$rules = [
			'embedded_link' => "required",
			'title' => "required",
			'description' => "required",
			'file' => "image",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This field is required.",
		];
	}
}