<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth, Input;
use App\Laravel\Requests\RequestManager;

class GalleryRequest extends RequestManager{

	public function rules(){

		$rules = [
			'album_id' => "required",
			'file' => "image|required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This field is required.",
		];
	}
}