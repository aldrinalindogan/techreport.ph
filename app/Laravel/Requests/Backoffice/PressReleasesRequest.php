<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth, Input;
use App\Laravel\Requests\RequestManager;

class PressReleasesRequest extends RequestManager{

	public function rules(){

		$rules = [
			'title' => "required",
			'content' => "required",
			'file' => "image|required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This field is required.",
		];
	}
}