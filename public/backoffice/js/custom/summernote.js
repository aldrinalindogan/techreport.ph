$(function(){
	$('#summernote').summernote({
    	height: 400,
    	onImageUpload: function(files, editor, welEditable) {
            sendFile(files[0], editor, welEditable);
        },
        fontNames: ['Roboto', 'Arial','Arial Black','Comic Sans MS','Helvetica','Impact','Tahoma','Times New Roman','Verdana'],
    });

     function sendFile(file, editor, welEditable) {
        data = new FormData();
        data.append("file", file);
        data.append("api_token","{{env('APP_TOKEN')}}");
        $.ajax({
            data: data,
            type: "POST",
            url: "{{route('api.summernote',['json'])}}",
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
            	if(data.status == true){
            		$('#summernote').summernote('insertImage', data.image);
            	}
            }
        });
    }
	    
});